#!/bin/bash

# Run the compilation toolchain
./compile.js --objs c programs/shell.c build/kernel.elf &&

# Prepare disc image
./mkbootable.sh build/kernel.elf build/bootable.img &&

# Run in QEMU
qemu-system-x86_64 -hda build/bootable.img