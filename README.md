# Straightforward Core Project  
  
![Straightforward Core Project](logo.png)
  
The Straightforward Core (or SFCore) is an educational operating system core with some basic programs included. It is forked from the [SimpleCore Kernel](https://gitlab.com/simplecore/kernel).

## Compilation workflow

### Prerequisites  

- A linux or mac system is currently required to run the shell scripts.  
- `nasm` to compile ASM files, `clang` and `gcc` to compile C files.
- Although free of external dependencies, node.js is required for the compile script.

### Flow

To compile with sfcsh (SFCore Shell):
```bash
./compile.js --objs c programs/shell.c build/kernel.elf
./mkbootable.sh build/kernel.elf build/bootable.img
```

You can now boot SFCore using QEMU:
```bash
qemu-system-x86_64 -hda build/bootable.img
```

For your convenience, a build-and-run script is provided (`run.sh`).

## TODO

- [X] ~~Replace the python build script with either a Node.js script (for [performance](https://benchmarksgame-team.pages.debian.net/benchmarksgame/fastest/node-python3.html) reasons) or a bash script (consistency with the other scripts)~~
- [ ] Uppercase keyboard input
- [ ] Support more programming languages!  
    - [ ] Rust: Would require Rust headers for each file — too messy, unless we're removing C program support.
    - [ ] C++: Seems painless — currently no need but a pull request adding support would get merged.