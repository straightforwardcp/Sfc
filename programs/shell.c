#include <sfcore.h>
#include <string.h>
#include <keyboard.h>
#include <printf.h>
#include <stdlib.h>
#include <consts.h>

#define SFCSH_VER "0.0.2"

int shell(void)
{
    char *cmd;
    char line[STR_BUFSIZE];

    while (1)
    {
        memset(line, 0, sizeof(line));
        printf("sfc $ ");
        if (!wgetuntil(line, '\n', 0, sizeof(line)))
            continue;

        if (strstartswith(line, "ccolor"))
        {
            char buf[STR_BUFSIZE];
            char subbuf[3];
            scursor_setcolor((int)strtol(&line[sizeof("ccolor")], NULL, 16));
            strcpy(buf, "The color is now ");
            memcpy(subbuf, &line[sizeof("ccolor")], 2); // Substring the first two characters
            subbuf[3] = '\0';
            strcat(buf, subbuf);
            sprintn(buf);
        }
        else if (strstartswith(line, "clear"))
        {
            sclear();
        }
        else if (strstartswith(line, "echo"))
        {
            sprintn(&line[sizeof("echo")]);
        }
        else if (strstartswith(line, "exit"))
        {
            sclear();
            halt("It is now safe to turn off your computer.\nIf you are using QEMU, you may use Ctrl + Alt + Q.");
        }
        else if (strstartswith(line, "help"))
        {
            sprintn("ccolor, clear, echo, exit, help, shellinfo");
        }
        else if (strstartswith(line, "shellinfo"))
        {
            char out[STR_BUFSIZE];
            strcpy(out, "sfcsh (SFCore Shell) ");
            strcat(out, SFCSH_VER);
            sprintn(out);
        }
        else
        {
            strrepl(line, 1, ' ', 0);
            printf("No such command: %s\n", line);
        }
    }
}

int program(void)
{
    sprint("Starting sfcsh...");
    sclear();

    shell();
}