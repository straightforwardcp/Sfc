#define LINES 25
#define COLUMNS 80
#define BYTES_FOR_EACH_ELEMENT 2
#define SCREENSIZE BYTES_FOR_EACH_ELEMENT *COLUMNS *LINES

/*
0 - Black
1 - Blue
2 - Green
3 - Cyan
4 - Red
5 - Magenta
6 - Brown
7 - Light Gray
8 - Gray
9 - Light Blue
a - Light Green
b - Light Cyan
c - Light Red
d - Light Magenta
e - Yellow
f - White
*/

// 0xbf (background, foreground)
#define DEFAULT_TEXT_COLOR 0x07
#define DEFAULT_CURSOR_COLOR 0x06
