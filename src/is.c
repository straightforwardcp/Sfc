int isalpha(int c)
{
    return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

int isdigit(int c)
{
    return (c >= '0' && c <= '9');
}

int isspace(int c)
{
    return (c == ' ' || c == '\t');
}

int isupper(int c)
{
    return (c >= 'A' && c <= 'Z');
}