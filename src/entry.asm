bits 64

section .stivalehdr
stivale_header:
    dq stack.top    ; rsp
    dw 0            ; video mode
    dw 0            ; fb_width
    dw 0            ; fb_height
    dw 0            ; bpp


section .data

global GDT64

; The GDT (Global descriptor table)
GDT64:
    .Null: equ $ - GDT64
    dq 0
    .Code: equ $ - GDT64
    dw 0
    dw 0
    db 0
    db 10011011b
    db 10101111b
    db 0
    .Data: equ $ - GDT64
    dw 0xFFFF
    dw 0
    db 0
    db 10010011b
    db 11001111b
    db 0
GDT_END:

GDT_PTR:
    dw GDT_END - GDT64 - 1    ; Limit
    dq GDT64                  ; Base

section .text

global exec_start
global port_inb
global port_outb
global load_idt
global disable_cursor

extern kmain 		;this is defined in sfcore.c
extern init_gdt

port_inb:
    xor eax, eax
    mov dx, di
    in al, dx
    ret

port_outb:
    mov dx, di
    mov al, sil
    out dx, al
    ret

load_idt:
    lidt [rdi]
    sti         ; Turn on interrupts
    ret

disable_cursor:
    pushf
    push rax
    push rdx

    mov dx, 0x3D4
    mov al, 0xA	; low cursor shape register
    out dx, al

    inc dx
    mov al, 0x20	; bits 6-7 unused, bit 5 disables the cursor, bits 0-4 control the cursor shape
    out dx, al

    pop rdx
    pop rax
    popf
    ret

exec_start:
    lgdt [GDT_PTR]

    ; Push the values for iretq
    push 0x10       ; ss
    push rsp        ; rsp
    pushf           ; rflags
    push 0x8        ; cs
    push run_kernel ; rip
    iretq

run_kernel:
    ; Run the kernel
    call kmain

    ; Halt if kmain returns
    cli
    hlt

section .bss
stack:
resb 4096; 4KB for stack
.top:
