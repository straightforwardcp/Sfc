#include <stdint.h>
#include "stivale_structs.h"

extern uint64_t cmdline_addr;
extern struct mmap_entry *read_stivale(struct stivale_struct *strct);
extern struct mmap_entry *mmap;
extern uint64_t mmap_entries;
