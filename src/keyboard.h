extern void idt_init(void);
extern void kb_init(void);
extern char kb_getchar(void);
extern unsigned int wgetuntil(char *buffer, char delimiter, char blockchar, unsigned int maxsize);
extern char getchar(void);
extern char getch(void);
