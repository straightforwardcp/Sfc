// Hidden thanks to qookie: https://github.com/qookei/quack/blob/master/kernel/lib/vsnprintf.h
#ifndef VSPRINTF_H
#define VSPRINTF_H

#include <stdarg.h>
#include <stdint.h>
#include <stddef.h>

extern void vsnprintf(char *buf, size_t len, const char *fmt, va_list arg);
extern void printf(const char *, ...);
extern void snprintf(char *buf, size_t len, const char *fmt, ...);

#endif
