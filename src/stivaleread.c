#include <stdint.h>
#include <stddef.h>
#include "stivale_structs.h"

uint64_t cmdline_addr;
struct mmap_entry *mmap;
uint64_t mmap_entries;

void read_stivale(struct stivale_struct* strct) {
    cmdline_addr = strct->cmdline;
    mmap = (struct mmap_entry *) strct->memory_map_addr;
    mmap_entries = strct->memory_map_entries;
}
