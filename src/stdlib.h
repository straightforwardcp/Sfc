#include "types.h"

extern char *itoa(int value, char *result, int base);
extern int atoi(const char *str);
extern long long strtol(const char *nptr, char **endptr, register int base);