#include <stddef.h>
#include <stdint.h>
#include "kbvars.h"
#include "keyboard.h"
#include "stdlib.h"
#include "string.h"
#include "stivale.h"
#include "asmfn.h"
#include "screendefs.h"
#include "consts.h"

// Video memory begins here.
static char *vidptr = (char *)0xb8000;

// Environmental stuff
static unsigned int cursor_location = 0;
static unsigned int cursor_column = 0;
static int cursor_color = 0x06;
//static unsigned int cursor_line = 0;
static unsigned int show_cursor = 1;
static unsigned int cursor_saves[10];
static unsigned int cursor_saves_c[sizeof(cursor_saves)];
//static unsigned int cursor_saves_l[sizeof(cursor_saves)];

// Define main program function
extern int program(void);

unsigned int spos_calc(unsigned int line, unsigned int column)
{
    return (COLUMNS * BYTES_FOR_EACH_ELEMENT * line) + (column * BYTES_FOR_EACH_ELEMENT);
}

unsigned int calc_center(unsigned int strlen)
{
    return (COLUMNS - strlen) / 2;
}

int scursor_save(unsigned int num)
{
    if (num > sizeof(cursor_saves) - 1)
    {
        return 0;
    }
    cursor_saves[num] = cursor_location;
    cursor_saves_c[num] = cursor_column;
    //cursor_saves_l[num] = cursor_line;
    return 1;
}

int scursor_restore(unsigned int num)
{
    if (num > sizeof(cursor_saves) - 1)
    {
        return 0;
    }
    cursor_location = cursor_saves[num];
    cursor_column = cursor_saves_c[num];
    //cursor_line = cursor_saves_l[num];
    return 1;
}

int scursor_getcolor(void)
{
    return cursor_color;
}

void scursor_setcolor(int newcolor)
{
    cursor_color = newcolor;
}

void scursor_update(void)
{
    if (show_cursor)
    {
        vidptr[cursor_location] = '_';
        vidptr[cursor_location + 1] = cursor_color;
    }
}

void scursor_remove(void)
{
    vidptr[cursor_location] = ' ';
    vidptr[cursor_location + 1] = 0x00;
}

void scursor_hide(void)
{
    scursor_remove();
    show_cursor = 0;
}

void scursor_show(void)
{
    show_cursor = 1;
    scursor_update();
}

void scursor_set(unsigned int line, unsigned int column)
{
    unsigned int modcursor = show_cursor;
    // Hide cursor
    if (modcursor)
        scursor_hide();
    // Set cursor column
    cursor_column = column;
    // Find and change to right position
    cursor_location = spos_calc(line, cursor_column);
    // Show cursor
    if (modcursor)
        scursor_show();
}

void scursor_center(unsigned int line, unsigned int strsize)
{
    unsigned int center_pos = calc_center(strsize);
    scursor_set(line, center_pos);
}

unsigned int sscroll(unsigned int lines)
{
    // Get amount of bytes to be "scrolled away"
    unsigned int backsize = COLUMNS * BYTES_FOR_EACH_ELEMENT * lines;
    // "Scroll" the bytes "away"
    for (unsigned int it = backsize; it != SCREENSIZE; it++)
    {
        vidptr[it - backsize] = vidptr[it];
    }
    // Clean new created space
    for (unsigned int it = 0; it != backsize; it++)
    {
        vidptr[SCREENSIZE - it] = 0;
    }
    // Return amount of bytes that were "scrolled away"
    return backsize;
}

void sclear(void)
{
    // there are 25 lines each of 80 COLUMNS; each element takes 2 bytes
    for (long it = 0; it < SCREENSIZE; it += 2)
    {
        // blank character
        vidptr[it] = ' ';
        // attribute-byte - black on black screen
        vidptr[it + 1] = 0x00;
    }
    // Update cursor to position 0
    cursor_location = 0;
    cursor_column = 0;
    scursor_update();
}

void sprint_back(void)
{
    if (cursor_column != 0)
    {
        scursor_remove();
        cursor_location -= 2;
        scursor_update();
        cursor_column--;
    }
}

void sprint_char(char character)
{
    if (character == 0)
    {
        return;
    }
    else if (character == '\b')
    {
        sprint_back();
        return;
    }
    else if (character == '\n')
    {
        scursor_remove();
        unsigned int line_size = BYTES_FOR_EACH_ELEMENT * COLUMNS;
        cursor_location = cursor_location + (line_size - cursor_location % (line_size));
        cursor_column = 0;
    }
    else
    {
        // the character's ascii
        vidptr[cursor_location++] = character;
        // Set character attribute
        vidptr[cursor_location++] = DEFAULT_TEXT_COLOR;
        // Increase position in line
        cursor_column++;
    }
    if (cursor_location == SCREENSIZE)
    {
        cursor_location -= sscroll(1);
    }
    scursor_update();
}

void sprint(const char *string)
{
    // this loop writes the string to video memory
    for (long it = 0; string[it] != 0; it++)
    {
        sprint_char(string[it]);
    }
}

void sprintn(const char *string)
{
    char str[STR_BUFSIZE];

    strcpy(str, string);
    strcat(str, "\n");
    sprint(str);
}

void scolor_set(char color)
{
    vidptr[cursor_location + 1] = color;
}

void panic(const char *message)
{
    sprint("\nKERNEL PANIC: ");
    sprint(message);
    // Block keyboard and hide cursor
    kb_block = 1;
    scursor_hide();
    // Finally block entire execution
    while (1)
        ;
}

// Like panic(), but does not show context
void halt(const char *message)
{
    sprint(message);
    kb_block = 1;
    scursor_hide();
    while (1)
        ;
}

void kmain(struct stivale_struct *strct)
{
    // Read stuff from stivale
    read_stivale(strct);
    // Disable cursor
    disable_cursor();
    // Clear screen
    sclear();
    // Initialise stuff
    scursor_update();
    idt_init();
    kb_init();
    // Run program
    int res = program();
    // Change to bottom line
    scursor_set(LINES - 1, 0);
    // Print result
    char smsg[] = "Program returned ";
    char exitcstr[3];
    itoa(res, exitcstr, 10);
    scursor_center(LINES - 1, sizeof(smsg) + sizeof(exitcstr));
    sprint(smsg);
    sprint(exitcstr);
    // Hide cursor
    scursor_hide();
    // Block everything
    kb_block = 1;
    while (1)
        ;
}
