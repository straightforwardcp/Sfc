#!/bin/bash

LANG=C

echo "Asking for root privileges via sudo..."
sudo true &&

echo "Creating empty image..." &&
dd if=/dev/zero of="$2" bs=1M count=30 status=none &&
sudo parted -s "$2" mklabel msdos &&
sudo parted -s "$2" mkpart primary 1 100% &&

echo "Downloading qloader2..." &&
wget -q -Obuild/qloader2-install https://raw.githubusercontent.com/qloader2/qloader2/fb827cb5bf3cc6f16b5ba6ce6bf202d935ac13ca/qloader2-install &&
wget -q -Obuild/qloader2.bin https://raw.githubusercontent.com/qloader2/qloader2/fb827cb5bf3cc6f16b5ba6ce6bf202d935ac13ca/qloader2.bin &&

echo "Installing qloader2 into image..." &&
chmod a+x build/qloader2-install &&
./build/qloader2-install build/qloader2.bin "$2" &&

echo "Adding files..." &&
(mkdir bootable_mount 2> /dev/null ; true) &&
loopfile="$(sudo losetup -Pf --show "$2")" &&
sudo mkfs.ext2 "${loopfile}p1" &&
sudo mount "${loopfile}p1" ./bootable_mount/ &&
sudo cp "$1" ./bootable_mount/kernel &&
sudo cp ./qloader2.cfg ./bootable_mount/
(sudo umount ./bootable_mount/ || sudo umount -l ./bootable_mount/) &&
(rmdir ./bootable_mount ; true) &&
sudo losetup -d "$loopfile" &&

echo "Built kernel in: build/kernel" &&


exit
