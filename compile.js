#!/usr/bin/node

const { execSync } = require("child_process")
const argv = process.argv.slice(1)
const fs = require("fs")
const os = require("os")

const cArgs = ["-m64", "-c", "-fno-stack-protector", "-fno-pic", "-mno-sse", "-mno-sse2", "-mno-mmx", "-mno-80387",
    "-mno-red-zone", "-mcmodel=kernel", "-ffreestanding", "-fno-stack-protector", "-fno-omit-frame-pointer"].join(" ");

const resetLang = () => {
    if (os.platform === "win32") return ""
    else return "LANG=C "
}

const call = (cmd) => {
    try {
        const out = execSync(cmd).toString()
        if (out) console.log(out)
    } catch (e) {
        panic(e.toString())
    }
}

const panic = (why) => {
    console.error("Error:", why)
    process.exit(1)
}

const logHeader = (content) => {
    console.log(`${"\033[37m"}${"-".repeat(35)}${"\033[0m"}\n${content}\n${"\033[37m"}${"-".repeat(35)}${"\033[0m"}`)
}

const objects = []
let objectsCompiled = false

if (argv.length > 1 && argv[1] === "--objs") {
    objectsCompiled = true
    logHeader("Compiling objects... 📦")
    fs.readdirSync("src").forEach(filename => {
        const filepath = `src/${filename}`
        const fileobj = `objects/${filename.split(".")[0]}.o`
        console.log(`Compiling ${filename}...`)
        if (filename.endsWith(".asm")) {
            objects.push(fileobj)
            call(`${resetLang()}nasm -f elf64 -o ${fileobj} ${filepath}`)
        } else if (filename.endsWith(".c")) {
            objects.push(fileobj)
            call(`${resetLang()}clang ${cArgs} -o ${fileobj} ${filepath}`)
        }
    })
    console.log("Done compiling objects!")
}

const usage = `${argv[0]} [--objs] <"c" or "obj"> <input> <output>`

if (argv.length < 3) {
    console.log("To link the kernel with a program\n", usage)
    process.exit(1)
}

if (!objectsCompiled) {
    // todo find out the reason this exists in the python original
}

if (!objects) {
    console.log("Please compile objects first")
    process.exit(1)
}

if (argv[2] === "c") {
    logHeader("Compiling user program... 🖥️")
    objects.push(`${argv[3].split(".")[0]}.o`)
    try {
        const gccOut = execSync(`${resetLang()}gcc ${cArgs} -I src -o ${objects[objects.length - 1]} ${argv[3]}`).toString()
        if (gccOut) console.log("[gcc]", gccOut)
    } catch (e) {
        console.log("[gcc]", e.toString())
    }

} else if (argv[2] === "obj") {
    objects.push(argv[2])
} else {
    console.log(`Invalid usage: Input type must be "c" or "obj":\n`, usage)
    process.exit(1)
}


const link = () => {
    console.log("Linking...")
    call(`ld -m elf_x86_64 -T link.ld -o ${argv[4]} ${objects.join(" ")}`)
    logHeader(`Kernel made at ${argv[4]}! 🎉`)
}

if (fs.existsSync(argv[4])) {
    fs.unlink(argv[4], () => {
        console.log("Removed existing kernel.")
        link()
    })
} else link()